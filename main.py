## Set up SDL DLL
import os
os.environ["PYSDL2_DLL_PATH"] = os.getcwd() # Force SDL2 DLL path to CWD (It's a workaround, but it works)

## Self imports
from main import *
from main.render import *

## Third party imports
from sdl2 import *
from OpenGL.GL import *
from numpy import *

## Python standard library imports
import ctypes
import math

## SECTIONS: (CTRL+F and search to locate)
## 1. GLOBAL VARIABLES
## 2. INITIALIZATION
## 3. SCENE CREATION
## 4. CALLBACK DEFINITIONS
## 5. SET CALLBACKS
## 6. UPDATE LOOP
## 7. DISPOSAL

##
## GLOBAL VARIABLES
##

## Constants for window size
WIDTH = 1024
HEIGHT = 768

## Update variables
frameElapsed = 0.0
elapsed = 0.0
speed = 32.0
frames = 0
pointSpeed = 1.0
frames = 0

## Input state

## Move directions
moveLeft = False
moveRight = False
moveForeward = False
moveBackward = False
moveUp = False
moveDown = False

## Turn
turnLeft = False
turnRight = False
turnUp = False
turnDown = False

## Wireframe mode
wireframe = False

## Joystick/Movement variables
joyPitch = 0.0
joyYaw = 0.0
joyHorizontal = 0.0
joyForward = 0.0
direction = [0.0, 0.0, 0.0]

##
## INITIALIZATION
##

## Start engine and open window
system.init()
w1 = window.Window(b"OpenGL Renderer", WIDTH, HEIGHT)
w1.makeCurrent() # Make window's GL context current

## Create camera
c = camera.Camera(WIDTH, HEIGHT)
c.getTransform().translate(0.0, 0.0, -4.0)

## Set up renderer
r = renderer.Renderer()
r.setCamera(c)
## Set the FOV and aspect ratio of the camera/renderer
r.setViewport(45.0, WIDTH, HEIGHT)
r.setGamma(1.0) # Set the gamma correction factor
## Set ambient light by ([r, g, b], intensity)
r.setAmbientLight([0.1, 0.1, 0.1], 0.2)

##
## SCENE CREATION
##

octree = octree.MeshOctree("res/meshes/bunny.obj", 6)
octree.getMaterial().setSpecFactor(8.0)
octree.getTransform().scaleu(4.0)

## Create and load mesh
mesh = mesh.Mesh("res/meshes/bunny.obj", True)
mesh.getTransform().translate(0.0, 2.0, 0.0)
mesh.getTransform().scaleu(1.0)
mesh.getMaterial().setSpecFactor(1.0)

## Create particle system
psys = psystem.ParticleSystem(512, "res/meshes/sphere.obj")
psys.getTransform().scaleu(0.1) # Scale to 0.1x
psys.getMaterial().setSpecFactor(8.0) # Set specular factor of 8.0

## Create and set up lights
l0 = light.Light(-2.0, +2.0, 0.0, atten=0.1) # Create first light
l1 = light.Light(+2.0, +2.0, 0.0, atten=0.1) # Create second light
l2 = light.Light(+0.0, -2.0, 0.0, atten=0.1) # Create third light
l0.setColor(1.0, 0.0, 0.0) # First light is red (R1, G0, B0)
l1.setColor(0.0, 1.0, 0.0) # Second light is green (R0, G1, B0)
l2.setColor(0.0, 0.0, 1.0) # Third light is blue (R0, G0, B1)

## Add meshes and lights
r.pushMeshes(mesh, octree, psys)
r.pushLights(l0, l1, l2)

##
## CALLBACK DEFINITIONS
##

##
## Calculate direction at which to move given speed
##
def calcMoveDir(delta, speed = 1.0, controllerspeed = 0.1, turnspeed = 2.0):
	## Import movement variables
	global moveForeward, moveBackward
	global moveLeft, moveRight
	global moveUp, moveDown
	global turnLeft, turnRight
	global turnUp, turnDown
	global joyPitch, joyYaw
	global joyHorizontal, joyForward
	## Initialize direction vector to 0.0
	dirX = 0.0
	dirY = 0.0
	dirZ = 0.0
	## Add to movement vector based on input
	if moveLeft: dirX -= delta * speed
	if moveRight: dirX += delta * speed
	if moveForeward: dirZ += delta * speed
	if moveBackward: dirZ -= delta * speed
	if moveUp: dirY += delta * speed
	if moveDown: dirY -= delta * speed
	if turnLeft: c.getTransform().rotate(0.0, delta * turnspeed, 0.0)
	if turnRight: c.getTransform().rotate(0.0, -delta * turnspeed, 0.0)
	if turnUp: c.getTransform().rotate(-delta * turnspeed, 0.0, 0.0)
	if turnDown: c.getTransform().rotate(delta * turnspeed, 0.0, 0.0)
	## Get direction transformed by look direction
	facingDir = c.getFacingDir([
		dirX + (joyHorizontal * controllerspeed),
		dirY,
		dirZ + (joyForward * controllerspeed)])
	## Rotate camera by rotation amount (Joystick only)
	c.getTransform().rotate(joyPitch * turnspeed, joyYaw * turnspeed, 0.0)
	return [facingDir[0]] + [-dirY] + [facingDir[2]]

def update(delta):
	## Import update variables
	global frameElapsed, frames, elapsed
	global pointX, pointY, pointZ, pointSpeed, maxRng
	## Add delta time to elapsed
	elapsed += delta
	frameElapsed += delta
	if frameElapsed >= 1.0:
		print(frames)
		frames = 0 # Reset frame counter
		frameElapsed = 0.0 # Reset frame time
	else:
		frames += 1

	## Move camera
	moveDir = calcMoveDir(delta, 10.0, controllerspeed=0.1, turnspeed=1.0)
	c.getTransform().translate(moveDir[0], moveDir[1], moveDir[2])

	## Transform objects
	octree.getTransform().rotate(0.0, delta * speed, 0.0)
	mesh.getTransform().rotate(0.0, delta * speed, 0.0)

	## Move particle system position in circular arc
	psys.setPoA((
		cos(elapsed * pointSpeed) * 100.0,
		sin(elapsed * pointSpeed) * 100.0,
		0.0))

##
## Handle user mouse movement
##
def mouseMoved(x, y, dX, dY):
	c.getTransform().rotate(dY / 10.0, dX / -10.0, 0.0)

##
## Handle key input event
##
def keyPress(pressed, repeated, keysym, mod):
	## Import movement variables
	global moveForeward, moveBackward
	global moveLeft, moveRight
	global moveUp, moveDown
	global turnLeft, turnRight
	global turnUp, turnDown
	global wireframe
	## Check scancode for input keys
	if keysym == SDLK_w: moveForeward = pressed
	if keysym == SDLK_s: moveBackward = pressed
	if keysym == SDLK_a: moveLeft = pressed
	if keysym == SDLK_d: moveRight = pressed
	if keysym == SDLK_SPACE: moveUp = pressed
	if keysym == SDLK_LCTRL: moveDown = pressed
	if keysym == SDLK_LEFT: turnLeft = pressed
	if keysym == SDLK_RIGHT: turnRight = pressed
	if keysym == SDLK_UP: turnUp = pressed
	if keysym == SDLK_DOWN: turnDown = pressed
	## Wireframe debug mode
	if keysym == SDLK_TAB and pressed:
		wireframe = not wireframe # Invert wireframe
		r.setWireframe(wireframe) # Set new wireframe state

##
## Handle Xbox 360 controller stick axis
##
def joyAxis(event, value):
	## Import joy axis variables
	global joyPitch, joyYaw
	global joyHorizontal, joyForward
	delta = window.getDelta() # Get current delta time
	## Handle for different sticks/axes
	if event == window.JOYAXIS_R_HORIZONTAL: joyYaw = -value
	if event == window.JOYAXIS_R_VERTICAL: joyPitch = value
	if event == window.JOYAXIS_L_HORIZONTAL: joyHorizontal = value
	if event == window.JOYAXIS_L_VERTICAL: joyForward = -value

##
## Handle Xbox 360 controller trigger pressed state
##
def joyTrigger(event, value): return

##
## Handle Xbox 360 controller button input
##
def joyButton(down, button):
	## Import selected movement variables
	global moveUp, moveDown, wireframe
	if button == window.JOYBUTTON_A: # Up on A pressed
		moveUp = down
	if button == window.JOYBUTTON_B: # Down on B pressed
		moveDown = down
	if button == window.JOYBUTTON_X: # Exit on X pressed
		system.quit() # Call system module
	if button == window.JOYBUTTON_Y and down: # Toggle wireframe
		wireframe = not wireframe
		r.setWireframe(wireframe)

##
## SET CALLBACKS
##

## Set up keyboard/mouse input
w1.setMouseMotionCallback(mouseMoved)
w1.setKeyCallback(keyPress)

## Grab mouse and set to relative
w1.setGrabMouse(True, True)

## Set up update callback
w1.setUpdateCallback(update)

## Set up controller input
w1.setJoyAxisCallback(joyAxis)
w1.setJoyTriggerCallback(joyTrigger)
w1.setJoyButtonCallback(joyButton)

##
## UPDATE LOOP
##

## Start the update loop
while(window.tick()):
	## Render scene
	r.render(window.getDelta())

##
## DISPOSAL
##

## Destroy window and renderer
w1.destroy()
r.dispose()
