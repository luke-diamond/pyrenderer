from . import window
from sdl2 import *
import atexit

stick = None

## Initialize system
def init():
	global stick
	## Initialize SDL
	SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK)
	if SDL_NumJoysticks() > 0:
		stick = SDL_JoystickOpen(0)
	## Register shutdown function
	atexit.register(lambda: quit())

##
## Handle program shutdown
##
def quit():
	## Deallocate SDL memory
	window.running = False
	SDL_Quit()
