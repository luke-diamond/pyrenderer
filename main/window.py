from sdl2 import *
import ctypes
from datetime import *

windows = []
running = True
delta = 0.0
activeWindow = None

## Joystick button constants
JOYBUTTON_A = 0
JOYBUTTON_B = 1
JOYBUTTON_X = 2
JOYBUTTON_Y = 3
JOYBUTTON_L_BUMPER = 4
JOYBUTTON_R_BUMPER = 5
JOYBUTTON_L_STICK = 6
JOYBUTTON_R_STICK = 7
JOYBUTTON_BACK = 8
JOYBUTTON_START = 9

## Joystick axis constants
JOYAXIS_L_HORIZONTAL = 0
JOYAXIS_L_VERTICAL = 1
JOYAXIS_R_HORIZONTAL = 2
JOYAXIS_R_VERTICAL = 3

## Joystick trigger constants
JOYTRIGGER_L = 0
JOYTRIGGER_R = 1

## Joystick D-pad constants
JOYDPAD_L = 0
JOYDPAD_R = 1
JOYDPAD_T = 2
JOYDPAD_B = 3
JOYDPAD_TL = 4
JOYDPAD_BL = 5
JOYDPAD_TR = 6
JOYDPAD_BR = 7
JOYDPAD_C = 8

class Window:
	__window = None
	__context = None
	__id = -1

	## Callbacks
	__updateCallback = None
	__mouseMotionCallback = None
	__keyCallback = None

	## Joystick callbacks
	__joyAxisCallback = None
	__joyTriggerCallback = None
	__joyButtonCallback = None
	__joyDPadCallback = None

	##
	## Initialize the window
	##
	def __init__(self, title, width, height):
		global windows
		## Create SDL window
		## Or together flags
		flags = 0
		flags |= SDL_WINDOW_SHOWN
		flags |= SDL_WINDOW_OPENGL
		self.__window = SDL_CreateWindow(
			title, # Window title
			SDL_WINDOWPOS_UNDEFINED, # Undefined window position
			SDL_WINDOWPOS_UNDEFINED, # Undefined window position
			width, # Window width
			height, # Window height
			flags) # Window flags
		## Enable antialiasing
		SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, b"1")
		## Create OpenGL context and set ID
		self.__context = SDL_GL_CreateContext(self.__window)
		self.__id = len(windows)
		windows += [self] # Add the window to the list

	##
	## Sets whether or not grab/capture the mouse
	##
	def setGrabMouse(self, relative, grab):
		SDL_SetRelativeMouseMode(SDL_TRUE if relative else SDL_FALSE)
		SDL_SetWindowGrab(self.__window, SDL_TRUE if grab else SDL_FALSE)

	##
	## Update the window
	##
	def update(self, delta):
		SDL_GL_SwapWindow(self.__window) # Swap window buffers
		self.__updateCallback(delta) # Call user-passed callback

	##
	## Set callback to be called on update
	##
	def setUpdateCallback(self, callback):
		self.__updateCallback = callback

	##
	## Facilitates mouse motion handling
	##
	def mouseMotion(self, x, y, xrel, yrel):
		if self.__mouseMotionCallback != None:
			self.__mouseMotionCallback(x, y, xrel, yrel)

	##
	## Set callback to be called on mouse motion
	##
	def setMouseMotionCallback(self, callback):
		self.__mouseMotionCallback = callback

	##
	## Facilitates key handling
	##
	def keyPress(self, pressed, repeat, keysym, mod):
		if self.__keyCallback != None:
			self.__keyCallback(pressed, repeat, keysym, mod)

	##
	## Set callback to be called on key press/release
	##
	def setKeyCallback(self, callback):
		self.__keyCallback = callback

	##
	## Facilitates joystick axis handling
	##
	def joyAxis(self, event, value):
		if self.__joyAxisCallback != None:
			self.__joyAxisCallback(event, value)

	##
	## Set callback to be called on joystick axis events
	##
	def setJoyAxisCallback(self, callback):
		self.__joyAxisCallback = callback

	##
	## Facilitates joystick trigger handling
	##
	def joyTrigger(self, event, value):
		if self.__joyTriggerCallback != None:
			self.__joyTriggerCallback(event, value)

	##
	## Set callback to be called on joystick trigger events
	##
	def setJoyTriggerCallback(self, callback):
		self.__joyTriggerCallback = callback

	##
	## Facilitates joystick button events
	##
	def joyButton(self, down, button):
		if self.__joyButtonCallback != None: self.__joyButtonCallback(down, button)

	##
	## Set callback to be called on joystick button events
	##
	def setJoyButtonCallback(self, callback): self.__joyButtonCallback = callback

	##
	## Facilitates D-pad events
	##
	def joyDPad(self, direction):
		if self.__joyDPadCallback != None: self.__joyDPadCallback(direction)

	##
	## Set callback to be called on D-pad press
	##
	def setJoyDPadCallback(self, callback): self.__joyDPadCallback = callback

	##
	## Make the current window's GL context current
	##
	def makeCurrent(self): SDL_GL_MakeCurrent(self.__window, self.__context)

	##
	## Get the window's index in the list
	##
	def getIndex(self): return self.__id

	##
	## Get the window's SDL ID
	##
	def getID(self): return SDL_GetWindowID(self.__window)

	##
	## Destroy the window
	##
	def destroy(self): SDL_DestroyWindow(self.__window)

##
## Search for window by ID
##
def getWindowByID(id):
	global windows # Import global windows
	for w in windows:
		if w.getID() == id: return w

##
## Get delta time of windows
##
def getDelta():
	global delta
	return delta

##
## The meat of the window system.
## Handles input, swaps frame buffer, and
## calls render methods.
##
def tick():
	global windows, running, delta, activeWindow
	event = SDL_Event()
	while SDL_PollEvent(ctypes.byref(event)) != 0:
		## Handle window events
		if event.type == SDL_WINDOWEVENT:
			## Window close
			if event.window.event == SDL_WINDOWEVENT_CLOSE:
				window = getWindowByID(event.window.windowID)
				windows.pop(window.getIndex())
				## Exit if no windows exists
				if len(windows) == 0:
					running = False
					break
			if event.window.event == SDL_WINDOWEVENT_FOCUS_GAINED:
				activeWindow = getWindowByID(event.window.windowID)
			if event.window.event == SDL_WINDOWEVENT_FOCUS_LOST:
				activeWindow = None
		## Handle keypress events
		if event.type == SDL_KEYDOWN or event.type == SDL_KEYUP:
			## Get window reference by ID
			window = getWindowByID(event.key.windowID)
			if event.key.keysym.sym == SDLK_ESCAPE:
				running = False
				break
			## Keypress state
			pressed = event.key.state == SDL_PRESSED
			repeat = event.key.repeat != 0
			keysym = event.key.keysym.sym
			mod = event.key.keysym.mod
			## Call window callback
			window.keyPress(pressed, repeat, keysym, mod)
		if event.type == SDL_MOUSEMOTION:
			window = getWindowByID(event.motion.windowID)
			if window != None:
				window.mouseMotion(
					event.motion.x, event.motion.y,
					event.motion.xrel, event.motion.yrel)
		if event.type == SDL_JOYAXISMOTION:
			if activeWindow != None:
				axis = event.jaxis.axis
				## Find value above threshold
				value = event.jaxis.value / 32767.0
				if abs(event.jaxis.value) <= 2400: value = 0.0
				## Stick event
				if axis == 0: # Horizontal left stick
					activeWindow.joyAxis(JOYAXIS_L_HORIZONTAL, value)
				if axis == 1: # Vertical left stick
					activeWindow.joyAxis(JOYAXIS_L_VERTICAL, value)
				if axis == 3: # Horizontal right stick
					activeWindow.joyAxis(JOYAXIS_R_HORIZONTAL, value)
				if axis == 4: # Vertical right stick
					activeWindow.joyAxis(JOYAXIS_R_VERTICAL, value)

				## Trigger event
				if axis == 2:
					activeWindow.joyTrigger(JOYTRIGGER_L, value)
				if axis == 5:
					activeWindow.joyTrigger(JOYTRIGGER_R, value)
		if event.type == SDL_JOYBUTTONDOWN or event.type == SDL_JOYBUTTONUP:
			if activeWindow != None:
				down = event.jbutton.state == SDL_PRESSED
				button = event.jbutton.button
				if button == 0: # A
					activeWindow.joyButton(down, JOYBUTTON_A)
				if button == 1: # B
					activeWindow.joyButton(down, JOYBUTTON_B)
				if button == 2: # X
					activeWindow.joyButton(down, JOYBUTTON_X)
				if button == 3: # Y
					activeWindow.joyButton(down, JOYBUTTON_Y)
				if button == 4: # Left bumper
					activeWindow.joyButton(down, JOYBUTTON_L_BUMPER)
				if button == 5: # Right bumper
					activeWindow.joyButton(down, JOYBUTTON_R_BUMPER)
				if button == 6: # Back
					activeWindow.joyButton(down, JOYBUTTON_BACK)
				if button == 7: # Start
					activeWindow.joyButton(down, JOYBUTTON_START)
				if button == 8: # Left stick
					activeWindow.joyButton(down, JOYBUTTON_L_STICK)
				if button == 9: # Right stick
					activeWindow.joyButton(down, JOYBUTTON_R_STICK)
		if event.type == SDL_JOYHATMOTION:
			if activeWindow != None:
				value = event.jhat.value
				if value == 0: # Center
					activeWindow.joyDPad(JOYDPAD_C)
				if value == 1: # Top
					activeWindow.joyDPad(JOYDPAD_T)
				if value == 2: # Right
					activeWindow.joyDPad(JOYDPAD_R)
				if value == 3: # Top right
					activeWindow.joyDPad(JOYDPAD_TR)
				if value == 4: # Bottom
					activeWindow.joyDPad(JOYDPAD_B)
				if value == 6: # Bottom right
					activeWindow.joyDPad(JOYDPAD_BR)
				if value == 8: # Left
					activeWindow.joyDPad(JOYDPAD_L)
				if value == 9: # Top left
					activeWindow.joyDPad(JOYDPAD_TL)
				if value == 12: # Bottom left
					activeWindow.joyDPad(JOYDPAD_BL)

	a = datetime.now() # Begin time
	for w in windows: w.update(delta)
	b = datetime.now() # End time
	diff = a - b # Calculate time difference
	if diff.microseconds > 0: delta = diff.microseconds / 1E8 # Calculate delta time
	## Return whether or not to continue running
	return running
