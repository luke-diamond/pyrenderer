import numpy
import math

##
## Decorator to pass as numpy array
##
def __npArrDecorator(x):
	return lambda *arg: numpy.array(x(*arg), dtype="float32")

##
## Calculate perspective projection matrix
##
@__npArrDecorator
def calcPerspectiveMatrix(fov, aspect, near, far):
	cot = lambda x: 1.0 / math.tan(x)
	hFOV = fov / 2.0 # Half FOV
	CFA = cot(math.radians(hFOV)) * aspect # Cotan FOV * Aspect
	CTF = cot(math.radians(hFOV)) # Cotan FOV
	NFZ = (far + near) / (far - near) # Near Far Z
	NFW = (2.0 * far * near) / (far - near) # Near far W
	return [
		[CFA, 0.0, 0.0, 0.0],
		[0.0, CTF, 0.0, 0.0],
		[0.0, 0.0, NFZ, NFW],
		[0.0, 0.0, -1.0, 0.0]
	]

##
## Calculate rotation matrix for
## the X axis.
##
@__npArrDecorator
def getRotMatX(deg):
	csX = math.cos(deg) # Cos deg
	snX = math.sin(deg) # Sin deg
	nsX = -math.sin(deg) # Negative sin deg
	return [
		[1.0, 0.0, 0.0, 0.0],
		[0.0, csX, nsX, 0.0],
		[0.0, snX, csX, 0.0],
		[0.0, 0.0, 0.0, 1.0]
	]

##
## Calculate rotation matrix for
## the Y axis.
##
@__npArrDecorator
def getRotMatY(deg):
	csX = math.cos(deg) # Cos deg
	snX = math.sin(deg) # Sin deg
	nsX = -math.sin(deg) # Negative sin deg
	return [
		[csX, 0.0, nsX, 0.0],
		[0.0, 1.0, 0.0, 0.0],
		[snX, 0.0, csX, 0.0],
		[0.0, 0.0, 0.0, 1.0]
	]


##
## Calculate the rotation matrix for
## the Z axis.
##
@__npArrDecorator
def getRotMatZ(deg):
	csX = math.cos(deg) # Cos deg
	snX = math.sin(deg) # Sin deg
	nsX = -math.sin(deg) # Negative sin deg
	return [
		[csX, nsX, 0.0, 0.0],
		[snX, csX, 0.0, 0.0],
		[0.0, 0.0, 1.0, 0.0],
		[0.0, 0.0, 0.0, 1.0]
		]

##
## Calculate translation matrix for all axes
##
@__npArrDecorator
def getTransMat(x, y, z):
	return [
		[1.0, 0.0, 0.0, x],
		[0.0, 1.0, 0.0, y],
		[0.0, 0.0, 1.0, z],
		[0.0, 0.0, 0.0, 1.0]
	]

##
## Calculate scaling matrix for all axes
##
@__npArrDecorator
def getScaleMat(x, y, z):
	return [
		[x, 0.0, 0.0, 0.0],
		[0.0, y, 0.0, 0.0],
		[0.0, 0.0, z, 0.0],
		[0.0, 0.0, 0.0, 1.0]
	]

class Transform:
	## Position
	__posX = 0.0
	__posY = 0.0
	__posZ = 0.0

	## Rotation
	__rotX = 0.0
	__rotY = 0.0
	__rotZ = 0.0

	__rotOffX = 0.0
	__rotOffY = 0.0
	__rotOffZ = 0.0

	## Scale
	__scaleX = 1.0
	__scaleY = 1.0
	__scaleZ = 1.0

	##
	## Initialize transform
	##
	def __init__(
			self,
			pos = [0.0, 0.0, 0.0],
			rot = [0.0, 0.0, 0.0],
			scale = [1.0, 1.0, 1.0]):
		self.__posX, self.__posY, self.__posZ = pos # Set position
		self.__rotX, self.__rotY, self.__rotZ = rot # Set rotation
		self.__scaleX, self__scaleY, self.__scaleZ = scale # Set scale

	##
	## Translate by vector
	##
	def translate(self, x, y, z):
		self.__posX += x
		self.__posY += y
		self.__posZ += z

	##
	## Rotate by vector
	##
	def rotate(self, x, y, z):
		self.__rotX += x
		self.__rotY += y
		self.__rotZ += z

	##
	## Scale by vector
	##
	def scale(self, x, y, z):
		self.__scaleX *= x
		self.__scaleY *= y
		self.__scaleZ *= z

	##
	## Scale by single, uniform float
	##
	def scaleu(self, x):
		self.__scaleX *= x
		self.__scaleY *= x
		self.__scaleZ *= x

	##
	## Set the position of the transform
	##
	def setPosition(self, x, y, z):
		self.__posX = x
		self.__posY = y
		self.__posZ = z

	##
	## Set the rotation offset of the transform
	##
	def setRotationOffset(self, x, y, z):
		self.__rotOffX = x
		self.__rotOffY = y
		self.__rotOffZ = z

	##
	## Set the rotation of the transform
	##
	def setRotation(self, x, y, z):
		self.__rotX = x
		self.__rotY = y
		self.__rotZ = z

	##
	## Set the scale of the transform
	##
	def setScale(self, x, y, z):
		self.__scaleX = x
		self.__scaleY = y
		self.__scaleZ = z

	##
	## Get the vector position of the transform
	##
	def getPosition(self):
		return [self.__posX, self.__posY, self.__posZ]

	##
	## Get the rotation offset of the transform
	##
	def getRotOffset(self):
		return [self.__rotOffX, self.__rotOffY, self.__rotOffZ]

	##
	## Get the vector rotation of the transform
	##
	def getRotation(self):
		return [self.__rotX, self.__rotY, self.__rotZ]

	##
	## Get the vector scale of the transform
	##
	def getScale(self):
		return [self.__scaleX, self.__scaleY, self.__scaleZ]

	##
	## Get translation matrix of the transform
	##
	def getTransMat(self):
		return getTransMat(self.__posX, self.__posY, self.__posZ)

	##
	## Get rotation offset matrix of the transform
	##
	def getRotOffMat(self):
		return getTransMat(self.__rotOffX, self.__rotOffY, self.__rotOffZ)

	##
	## Get rotation matrix from transform
	##
	def getRotMat(self):
		## Multiply Z*Y*X (Matrix multiplication is flipped)
		return numpy.matmul(
			numpy.matmul(
				getRotMatX(math.radians(self.__rotX)),
				getRotMatY(math.radians(self.__rotY))),
				getRotMatZ(math.radians(self.__rotZ)))

	##
	## Get scale matrix from transform
	##
	def getScaleMat(self):
		return getScaleMat(self.__scaleX, self.__scaleY, self.__scaleZ)

	##
	## Get combined result of the translation,
	## rotation, and scale matrices.
	##
	def getCombined(self, flatten):
		## S*R*T flips to T*R*S for matrix multiplication
		combined = numpy.matmul(
			numpy.matmul(
				numpy.matmul(
					self.getTransMat(),
					self.getRotOffMat()),
				self.getRotMat()),
				self.getScaleMat())
		if flatten:
			return combined.flatten()
		else:
			return combined

	##
	## Get combined result of the translation and
	## rotation matrices, for the view matrix.
	##
	def getCombinedView(self, flatten):
		combined = numpy.matmul(
			numpy.matmul(
				self.getRotOffMat(), self.getRotMat()),
			self.getTransMat())
		if flatten:
			return combined.flatten()
		else:
			return combined
