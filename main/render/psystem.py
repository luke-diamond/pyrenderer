from . import mesh

import random
import math

class ParticleSystem(mesh.Mesh):

	__instOffsets = [] # Particle offsets
	__PoA = [0.0, 0.0, 0.0] # Point of Attraction

	##
	## Initialize the particle system
	##
	def __init__(self, instances, partFile, smooth = False):
		super().__init__(partFile, smooth) # Initialize parent mesh
		## Populate particles
		for _ in range(0, instances): self.__instOffsets += [[0.0, 0.0, 0.0]]
		## Set particle instances
		self.setInstanceOffsets(self.__instOffsets)

	##
	## Set the system's Point of Attraction
	##
	def setPoA(self, point): self.__PoA = point

	## Lambdas for update
	__length = lambda x: math.sqrt(x[0]**2.0 + x[1]**2.0 + x[2]**2.0)
	__jitter = lambda: random.uniform(-1.0, 1.0)
	def draw(self, delta):
		## Loop through and update particles
		for i in range(0, len(self.__instOffsets)):
			## Calculate distance vector and length
			d = [x - y for x, y in zip(self.__PoA, self.__instOffsets[i])]
			dL = ParticleSystem.__length(d)
			if dL > 0.0: d = [x / dL for x in d] # Normalize distance vector

			## Move particles towards PoA with jitter
			self.__instOffsets[i][0] += d[0] - ParticleSystem.__jitter()
			self.__instOffsets[i][1] += d[1] - ParticleSystem.__jitter()
			self.__instOffsets[i][2] += d[2] - ParticleSystem.__jitter()

		self.setInstanceOffsets(self.__instOffsets) # Update instanced mesh
		super().draw(delta) # Draw elements
