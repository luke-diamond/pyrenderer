class Light:
	__position = [0.0, 0.0, 0.0]
	__color = [1.0, 1.0, 1.0]
	__intensity = 1.0
	__attenuation = 1.0

	##
	## Initialize light
	##
	def __init__(self, x, y, z, bri=1.0, atten=1.0):
		self.__position = [x, y, z]
		self.__intensity = bri
		self.__attenuation = atten

	##
	## Return the position of the light
	##
	def getPosition(self): return self.__position

	##
	## Set the position of the light
	##
	def setPosition(self, x, y, z): self.__position = [x, y, z]

	##
	## Translate the light by a vector
	##
	def translate(self, x, y, z):
		self.__position[0] += x # X' = X+Tx
		self.__position[1] += y # Y' = Y+Ty
		self.__position[2] += z # Z' = Z+Tz

	##
	## Set the intensity of the light
	##
	def setIntensity(self, intensity): self.__intensity = intensity

	##
	## Get the intensity of the light
	##
	def getIntensity(self): return self.__intensity

	##
	## Set the attenuation factor of the light
	##
	def setAttenuation(self, attenuation): self.__attenuation = attenuation

	##
	## Get the attenuation factor of the light
	##
	def getAttenuation(self): return self.__attenuation

	##
	## Set the emissive color of the light
	##
	def setColor(self, r, g, b): self.__color = [r, g, b]

	##
	## Get the emissive color of the light
	##
	def getColor(self): return self.__color
