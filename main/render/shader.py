from OpenGL.GL import *

class Shader:
	__programID = -1

	##
	## Load and compile shader
	##
	def __init__(self, vertPath, fragPath):
		## Open shader files
		vertFile = open(vertPath, "r")
		fragFile = open(fragPath, "r")

		## Read shader source
		vertSource = vertFile.read()
		fragSource = fragFile.read()

		## Close shader files
		vertFile.close()
		fragFile.close()

		## Create OpenGL shaders
		vert = glCreateShader(GL_VERTEX_SHADER)
		frag = glCreateShader(GL_FRAGMENT_SHADER)

		## Compile vertex shader
		glShaderSource(vert, vertSource)
		glCompileShader(vert)
		## Check for compilation error
		err = glGetShaderiv(vert, GL_COMPILE_STATUS)
		if err == GL_FALSE:
			print("VERTEX SHADER COMPILATION FAILED")
			print(glGetShaderInfoLog(vert))
		assert err == GL_TRUE # Ensure shader compiled

		## Compile fragment shader
		glShaderSource(frag, fragSource)
		glCompileShader(frag)
		## Check for compilation error
		err = glGetShaderiv(frag, GL_COMPILE_STATUS)
		if err == GL_FALSE:
			print("FRAGMENT SHADER COMPILATION FAILED")
			print(glGetShaderInfoLog(frag))
		assert err == GL_TRUE # Ensure shader compiled

		## Create OpenGL shader program
		self.__programID = glCreateProgram()
		## Attach vert/frag shaders and link
		glAttachShader(self.__programID, vert)
		glAttachShader(self.__programID, frag)
		glLinkProgram(self.__programID)
		## Check for compilation errors
		err = glGetProgramiv(self.__programID, GL_LINK_STATUS)
		if err == GL_FALSE:
			print("PROGRAM LINK FAILED")
			print(glGetProgramInfoLog(self.__programID))
		assert err == GL_TRUE # Ensure shader compiled

		## Delete vert/frag shaders
		glDeleteShader(vert)
		glDeleteShader(frag)

	##
	## Bind attribute name to location
	##
	def bindAttribLoc(self, name, loc): 
		glBindAttribLocation(self.__programID, loc, name)

	##
	## Get numerical location of uniform by name
	##
	def getUniformLoc(self, name): 
		return glGetUniformLocation(self.__programID, name)

	##
	## Delete the shader program
	##
	def delete(self):
		if bool(glDeleteProgram): glDeleteProgram(self.__programID)

	##
	## Bind the shader program for use
	##
	def bind(self): glUseProgram(self.__programID)

	##
	## Unbind the shader program
	##
	def unbind(self): glUseProgram(0)
