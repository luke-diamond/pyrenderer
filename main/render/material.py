from OpenGL.GL import *
import numpy

class Material:
	__diffuseColor = [0.0, 0.0, 0.0]
	__specFactor = 0.0

	##
	## Initialize the material.
	## All arguments are optional.
	##
	def __init__(self, difCol = [0.0, 0.0, 0.0], specFac = 0.0):
		self.__diffuseColor = difCol
		self.__specFactor = specFac

	##
	## Pass material's uniforms to
	## a given shader.
	##
	def passUniforms(self, shader):
		glUniform3fv(
			shader.getUniformLoc("u_DiffuseColor"),
			1,
			numpy.array(self.__diffuseColor, dtype="float32"))
		glUniform1f(shader.getUniformLoc("u_SpecFac"), self.__specFactor)

	##
	## Set the diffuse color of the material
	##
	def setDiffuseColor(self, col): self.__diffuseColor = col

	##
	## Set the specular factor of the material
	##
	def setSpecFactor(self, fac): self.__specFactor = fac

	##
	## Return the diffuse color of the material
	##
	def getDiffuseColor(self): return self.__diffuseColor

	##
	## Return the specular factor of the material
	##
	def getSpecFactor(self): return self.__specFactor
