from . import transform

import numpy
import numpy.linalg

class Camera(object):
	__transform = None
	__projection = None
	__fov = 0.0
	__width = 0.0
	__height = 0.0
	__near = -1.0
	__far = 1.0

	##
	## Recalculate projection matrix
	##
	def __recalculateProjection(self):
		self.__projection = transform.calcPerspectiveMatrix(
			self.__fov,
			self.__height / self.__width,
			self.__near,
			self.__far)

	##
	## Initialize the camera
	##
	def __init__(self, width, height, fov = 45.0):
		self.__transform = transform.Transform() # Create transform
		## Set camera properties
		self.__fov = fov
		self.__width = width
		self.__height = height
		## Calculate projection
		self.__recalculateProjection()

	##
	## Get a reference to the camera transform
	##
	def getTransform(self): return self.__transform

	##
	## Get the camera projection matrix
	##
	def getProjection(self): return self.__projection

	##
	## Set the transform of the camera
	##
	def setTransform(self, transform): self.__transform = transform

	##
	## Set the projection matrix of the camera
	##
	def setProjection(self, projection): self.__projection = projection

	##
	## Adjust the camera on screen scale
	##
	def resize(self, width, height, fov=None):
		## Reset camera properties
		self.__width = width
		self.__height = height
		## Set FOV if passed
		if fov != None: self.__fov = fov
		## Recalculate projection
		self.__recalculateProjection()

	##
	## Set the FOV of the camera
	##
	def setFOV(self, fov):
		self.__fov = fov
		self.__recalculateProjection()

	##
	## Convert world direction to camera direction
	##
	def getFacingDir(self, dir):
		## Get direction in world space
		dirVec = numpy.array(
			[-dir[0], -dir[1], dir[2]] + [0.0],
			dtype="float32")
		## Get camera view rotation
		view = self.getTransform().getRotMat()
		## Return dot product
		return (dirVec.dot(view)).flatten()[:3]
