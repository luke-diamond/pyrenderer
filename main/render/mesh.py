from . import transform
from . import material
from OpenGL.GL import *
import numpy
import math
import re

##
## Calculates the surface normal of 3 points
##
def calcSurfaceNormal(x, y, z):
	u = [b - a for a, b in zip(x, y)] # U = Vb - Va
	v = [c - a for a, c in zip(x, z)] # V = Vc - Va
	norm = [0.0] * 3
	norm[0] = (u[1] * v[2]) - (u[2] * v[1]) # X = UyVz - UzVy
	norm[1] = (u[2] * v[0]) - (u[0] * v[2]) # Y = UzVx - UxVz
	norm[2] = (u[0] * v[1]) - (u[1] * v[0]) # Z = UxVy - UyVx
	return norm

##
## Facilitates the reading of an OBJ file
##
def getOBJData(meshFile):
	verts = []
	normals = []
	texcoords = []
	vertexElements = []
	normalElements = []
	textureElements = []

	## Regex programs
	vProg = re.compile("v\s([-]?\d.+)\s([-]?\d.+)\s([-]?\d.+)")
	nProg = re.compile("vn\s([-]?\d.+)\s([-]?\d.+)\s([-]?\d.+)")
	tcProg = re.compile("vt\s([-]?\d.+)\s([-]?\d.+)\s([-]?\d.+)")

	fVProg = re.compile("\s?(\d+)\s?")
	fVNProg = re.compile("\s?(\d+)//(\d+)\s?")
	fVTProg = re.compile("\s?(\d+)/(\d+)/\s?")
	fVTNProg = re.compile("\s?(\d+)/(\d+)/(\d+)\s?")

	## Parse file, loading into data stores
	with open(meshFile) as f:
		for l in f: # Loop line-by-line through file
			## Face definiton
			if l.startswith("f "):
				## Vertex, texture, and normal
				if re.search(fVTNProg, l):
					match = re.findall(fVTNProg, l)
					## Add values to data store
					for (ve, te, ne) in match:
						vertexElements += [ve]
						textureElements += [te]
						normalElements += [ne]
				## Vertex and normal
				elif re.search(fVNProg, l):
					match = re.findall(fVNProg, l)
					## Add values to data store
					for (ve, ne) in match:
						vertexElements += [ve]
						normalElements += [ne]
				## Vertex and texture
				elif re.search(fVTProg, l):
					match = re.findall(fVTProg, l)
					## Add values to data store
					for (ve, te) in match:
						vertexElements += [ve]
						textureElements += [te]
				## Only vertex
				elif re.search(fVProg, l):
					match = re.findall(fVProg, l)
					## Add values to data store
					for (ve) in match:
						vertexElements += [ve]
			## Vertex
			if re.search(vProg, l):
				match = re.findall(vProg, l)
				for (x, y, z) in match:
					verts += [[float(x), float(y), float(z)]]
			## Normal vector
			elif re.search(nProg, l):
				match = re.findall(nProg, l)
				for (x, y, z) in match:
					normals += [[float(x), float(y), float(z)]]
			## Texture coordinate vector
			elif re.search(tcProg, l):
				match = re.findall(tcProg, l)
				for (x, y, z) in match:
					texcoords += [[float(x), float(y), float(z)]]
	return (
		verts, normals, texcoords,
		vertexElements, normalElements, textureElements)

class Mesh:
	__vao = -1
	__vertVBO = -1
	__normVBO = -1
	__instanceOffsetVBO = -1
	__numElems = 0
	__material = None
	__transform = None
	__instances = 1

	def __init__(self, meshFile, smooth):
		self.__material = material.Material()
		self.__transform = transform.Transform()

		(verts, normals, texcoords,
		vertexElements, normalElements, textureElements) = getOBJData(meshFile)

		## Set up mesh state
		self.__vao = glGenVertexArrays(1)
		self.__vertVBO = glGenBuffers(1)
		self.__normVBO = glGenBuffers(1)
		self.__instanceOffsetVBO = glGenBuffers(1)
		self.__numElems = len(vertexElements)

		## Loop through vertex elements
		for i in range(0, len(vertexElements)):
			vertexElements[i] = int(vertexElements[i]) - 1

		orderedVerts = []
		orderedNorms = []
		finalNorms = []

		vertNorms = dict()

		## Order vertices
		for x in vertexElements: orderedVerts += verts[x]
		## Calculate surface normals
		for i in range(0, len(vertexElements), 3):
			e0, e1, e2 = vertexElements[i:i+3]
			v0, v1, v2 = verts[e0], verts[e1], verts[e2]
			surfNorm = calcSurfaceNormal(v0, v1, v2)
			if smooth:
				if str(v0) not in vertNorms:
					vertNorms[str(v0)] = [[0.0, 0.0, 0.0], 0]
				if str(v1) not in vertNorms:
					vertNorms[str(v1)] = [[0.0, 0.0, 0.0], 0]
				if str(v2) not in vertNorms:
					vertNorms[str(v2)] = [[0.0, 0.0, 0.0], 0]
				v0e = vertNorms[str(v0)]
				v1e = vertNorms[str(v1)]
				v2e = vertNorms[str(v2)]

				## Add surface normal to vertex dictionary
				v0e[0][0] += surfNorm[0]
				v0e[0][1] += surfNorm[1]
				v0e[0][2] += surfNorm[2]
				v1e[0][0] += surfNorm[0]
				v1e[0][1] += surfNorm[1]
				v1e[0][2] += surfNorm[2]
				v2e[0][0] += surfNorm[0]
				v2e[0][2] += surfNorm[2]
				v2e[0][1] += surfNorm[1]

				## Increment vertex normal count
				v0e[1] += 1
				v1e[1] += 1
				v2e[1] += 1
			else:
				orderedNorms += [surfNorm] * 3
		if smooth:
			## Loop through vertex/normal/amount triplets and
			## calculate smoothed vertex normals
			for e in vertexElements:
				vert = verts[e]
				## If vertex has no normals, skip
				if vertNorms[str(vert)][1] == 0:
					finalNorms += [[0.0, 0.0, 0.0]]
					continue
				else:
					## Normalize normal
					v = vertNorms[str(vert)]
					nX = v[0][0] / v[1]
					nY = v[0][1] / v[1]
					nZ = v[0][2] / v[1]
					finalNorms += [[nX, nY, nZ]]

		## Bind VAO
		glBindVertexArray(self.__vao)

		## Enable attributes
		glEnableVertexAttribArray(0) # Attrib 0 -> Vert Pos
		glEnableVertexAttribArray(1) # Attrib 1 -> Vert Norm
		glEnableVertexAttribArray(2) # Attrib 2 -> Instance Offset

		## Set up vertex VBO
		glBindBuffer(GL_ARRAY_BUFFER, self.__vertVBO)
		glBufferData(
			GL_ARRAY_BUFFER,
			numpy.array(
				orderedVerts,
				dtype="float32").flatten(),
			GL_STATIC_DRAW)
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, None)
		glBindBuffer(GL_ARRAY_BUFFER, 0)

		## Set up normal VBO
		glBindBuffer(GL_ARRAY_BUFFER, self.__normVBO)
		glBufferData(
			GL_ARRAY_BUFFER,
			numpy.array(
				finalNorms if smooth else orderedNorms,
				dtype="float32").flatten(),
			GL_STATIC_DRAW)
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, None)
		glBindBuffer(GL_ARRAY_BUFFER, 0)

		## Set up default instance offset VBO
		glBindBuffer(GL_ARRAY_BUFFER, self.__instanceOffsetVBO)
		glBufferData(
			GL_ARRAY_BUFFER,
			numpy.array(
				[0.0, 0.0, 0.0],
				dtype="float32").flatten(),
			GL_DYNAMIC_DRAW)
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, None)
		glVertexAttribDivisor(2, 1)
		glBindBuffer(GL_ARRAY_BUFFER, 0)

		## Unbind VAO
		glBindVertexArray(0)

	##
	## Re-buffer the instance positions for the
	## mesh to be drawn with.
	##
	def setInstanceOffsets(self, positions):
		self.__instances = len(positions)
		glBindVertexArray(self.__vao)

		glBindBuffer(GL_ARRAY_BUFFER, self.__instanceOffsetVBO)
		glBufferData(
			GL_ARRAY_BUFFER,
			numpy.array(
				positions, dtype="float32").flatten(),
			GL_STREAM_DRAW)
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, None)
		glVertexAttribDivisor(2, 1)
		glBindBuffer(GL_ARRAY_BUFFER, 0)

		glBindVertexArray(0)

	##
	## Set the material of the mesh
	##
	def setMaterial(self, mat): self.__material = mat

	##
	## Get the material of the mesh
	##
	def getMaterial(self): return self.__material

	##
	## Set the transform of the mesh
	##
	def setTransform(self, trans): self.__transform = trans

	##
	## Get the transform of the mesh
	##
	def getTransform(self): return self.__transform

	##
	## Set the number of instances of the mesh to draw
	##
	def setInstances(self, instances): self.__instances = instances

	##
	## Return the number of instances of the mesh to draw
	##
	def getInstances(self): return self.__instances

	##
	## Draw the mesh
	##
	def draw(self, delta):
		glBindVertexArray(self.__vao)
		## Draw n triangles
		glDrawArraysInstanced(
			GL_TRIANGLES, # Draw array of triangl verts
			0, # Start at 0
			self.__numElems, # N triangles in array
			self.__instances) # Number of instances to draw
		glBindVertexArray(0)

	##
	## Dispose of the mesh
	##
	def dispose(self):
		try:
			glDeleteVertexArrays(1, self.__vao)
			glDeleteBuffers(1, self.__vertVBO)
			glDeleteBuffers(1, self.__normVBO)
			glDeleteBuffers(1, self.__instanceOffsetVBO)
		except: pass
