from . import mesh
from . import shader
from . import transform

import numpy
from OpenGL.GL import *

class Renderer:
	__meshes = []
	__lights = []
	__shader = None
	__gamma = 2.3
	__ambientColor = [0.0, 0.0, 0.0, 0.0]
	__ambientIntensity = 1.0
	__camera = None

	##
	## Initialize renderer and set up required state
	##
	def __init__(self):
		## Load shader
		self.__shader = shader.Shader(
			"res/shaders/vert.glsl",
			"res/shaders/frag.glsl")
		## Bind attributes for use with meshes
		self.__shader.bindAttribLoc("in_Pos", 0)
		self.__shader.bindAttribLoc("in_Norm", 1)
		self.__shader.bindAttribLoc("in_InstPos", 2)
		## Set up OpenGL state
		glEnable(GL_DEPTH_TEST) # Enable Z test
		glDepthFunc(GL_LESS) # Z1 <= Z0
		glEnable(GL_DEPTH_CLAMP) # Enable Z clamp
		glEnable(GL_CULL_FACE) # Enable backface culling
		glCullFace(GL_BACK)

	##
	## Sets whether to fill polygons
	##
	def setWireframe(self, wireframe):
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE if wireframe else GL_FILL)

	##
	## Set the current camera of the renderer
	##
	def setCamera(self, camera):
		self.__camera = camera

	##
	## Change the renderer viewport
	##
	def setViewport(self, fov, w, h):
		self.__camera.resize(w, h, fov)

	##
	## Add mesh to render list
	##
	def pushMeshes(self, *meshes): self.__meshes += [m for m in meshes]

	##
	## Remove mesh from render list
	##
	def removeMesh(self, mesh): self.__meshes.remove(mesh)

	##
	## Add light to the list
	##
	def pushLights(self, *lights): self.__lights += [l for l in lights]

	##
	## Remove a light from the list
	##
	def removeLight(self, light): self.__lights.remove(light)

	##
	## Set the gamma correction factor
	##
	def setGamma(self, gamma): self.__gamma = gamma

	##
	## Get the gamma correction factor
	##
	def getGamma(self): return self.__gamma

	##
	## Set the ambient light and intensity
	##
	def setAmbientLight(self, color, intensity):
		self.__ambientColor = color
		self.__ambientIntensity = intensity

	##
	## Get a tuple of the ambient light + intensity
	##
	def getAmbientLight(self):
		return (self.__ambientColor, self.__ambientIntensity)

	##
	## Render the scene
	##
	def render(self, delta):
		## Clear color/depth to black
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
		glClearColor(
			self.__ambientColor[0], # Red component
			self.__ambientColor[1], # Green component
			self.__ambientColor[2], # Blue component
			1.0)


		## Loop through and draw meshes
		for mesh in self.__meshes:
			self.__shader.bind() # Bind shader
			## Loop through lights
			for i, light in enumerate(self.__lights):
				## Pass light uniforms
				## Pass position (vec3), color (vec3),
				## intensity (float), and attenuation (float).
				glUniform3fv( # Light position
					self.__shader.getUniformLoc(
						"u_Lights[" + str(i) + "].position"),
					1,
					numpy.array(light.getPosition(), dtype="float32"))
				glUniform3fv( # Light color
					self.__shader.getUniformLoc(
						"u_Lights[" + str(i) + "].color"),
					1,
					numpy.array(light.getColor(), dtype="float32"))
				glUniform1f( # Light intensity
					self.__shader.getUniformLoc(
						"u_Lights[" + str(i) + "].intensity"),
					light.getIntensity())
				glUniform1f( # Light attenuation
					self.__shader.getUniformLoc(
						"u_Lights[" + str(i) + "].attenuation"),
					light.getAttenuation())
				glUniform1i( # Light count
					self.__shader.getUniformLoc("u_LightCount"),
					len(self.__lights)) # Pass light count
				glUniform1f( # Light count inverse (optimization)
					self.__shader.getUniformLoc("u_LightCountInverse"),
					1.0 / len(self.__lights))

			## Pass ambient light uniforms
			glUniform3fv( # Ambient color
				self.__shader.getUniformLoc("u_AmbientColor"),
				1,
				numpy.array(self.__ambientColor, dtype="float32"))
			glUniform1f( # Ambient intensity
				self.__shader.getUniformLoc("u_AmbientIntensity"),
				self.__ambientIntensity)
			glUniform3fv( # Ambient combined (optimization)
				self.__shader.getUniformLoc("u_Ambient"),
				1,
				numpy.array(
					[x * self.__ambientIntensity for x in self.__ambientColor],
					dtype="float32"))

			## Pass gamma correction factor uniform
			glUniform1f(
				self.__shader.getUniformLoc("u_Gamma"), 1.0 / self.__gamma)

			## Pass material parameters to shader
			mesh.getMaterial().passUniforms(self.__shader)

			## Pass M/V/P matrices to shader
			glUniformMatrix4fv( # Model matrix
				self.__shader.getUniformLoc("u_M"),
				1,
				GL_TRUE,
				mesh.getTransform().getCombined(True))
			glUniformMatrix4fv( # View matrix
				self.__shader.getUniformLoc("u_V"),
				1, GL_TRUE,
				self.__camera.getTransform().getCombinedView(True))
			glUniformMatrix4fv( # Projection matrix
				self.__shader.getUniformLoc("u_P"),
				1,
				GL_TRUE,
				self.__camera.getProjection())

			mesh.draw(delta) # Draw mesh

			self.__shader.unbind() # Unbind shader

	##
	## Dispose of the renderer and its resources
	##
	def dispose(self):
		self.__shader.delete() # Delete shader
		[m.dispose() for m in self.__meshes] # Dispose of meshes
