from . import mesh
from . import material

import math

sizeByDepth = lambda depth: 1.0/(2.0**depth) # Get octant size by depth in octree

##
## Check whether point clips with box
##
def clipPoint(boxpos, boxsize, point):
	bX, bY, bZ = boxpos # Unwrap box pos
	bsX, bsY, bsZ = boxsize # Unwrap box size
	pX, pY, pZ = point # Unwrap point
	return (
		bX + bsX >= pX and bX <= pX and # Test X-plane intersection
		bY + bsY >= pY and bY <= pY and # Test Y-plane intersection
		bZ + bsZ >= pZ and bZ <= pZ # Test Z-plane intersection
		)

class Octree:
	__root = None
	__depth = -1

	##
	## Leaf positions by index
	##
	## 1 -> Up top left
	## 2 -> Up top right
	## 3 -> Up bottom right
	## 4 -> Up bottom left

	## 5 -> Down top left
	## 6 -> Down top right
	## 7 -> Down bottom right
	## 8 -> Down bottom left

	class Leaf:
		__leafNodes = []
		__x = 0.0
		__y = 0.0
		__z = 0.0
		__sx = 0.0
		__sy = 0.0
		__sz = 0.0
		__depth = 0
		__count = 0

		##
		## Initialize octant leaf
		##
		def __init__(self, px, py, pz, sx, sy, sz, depth = 0):
			self.__depth = depth
			self.__x, self.__y, self.__z = px, py, pz # Set octant position
			self.__sx, self.__sy, self.__sz = sx, sy, sz # Set octant size

		def getPos(self): return [self.__x, self.__y, self.__z]

		def getSize(self): return [self.__sx, self.__sy, self.__sz]

		def getOffsetPos(self):
			return [
					self.__x / (self.__sx * 2.0),
					self.__y / (self.__sy * 2.0),
					self.__z / (self.__sz * 2.0)
				]

		##
		## Populate the leaf with 8 children
		##
		def populateLeaf(self):
			cpx, cpy, cpz = self.__x, self.__y, self.__z # Set position
			csx, csy, csz = [sizeByDepth(self.__depth)] * 3 # Set size vars
			## Subdivide the octree leaf
			## Upper octants
			self.__leafNodes = [
				self.__class__( # 1 -> Up top left
					cpx, cpy + csy, cpz + csz,
					csx, csy, csz,
					self.__depth + 1),
				self.__class__( # 2 -> Up top right
					cpx + csx, cpy + csy, cpz + csz,
					csx, csy, csz,
					self.__depth + 1),
				self.__class__( # 3 -> Up bottom right
					cpx + csx, cpy + csy, cpz,
					csx, csy, csz,
					self.__depth + 1),
				self.__class__( # 4 -> Up bottom left
					cpx, cpy + csy, cpz,
					csx, csy, csz,
					self.__depth + 1),
				## Lower octants
				self.__class__( # 5 -> Down top left
					cpx, cpy, cpz + csz,
					csx, csy, csz,
					self.__depth + 1),
				self.__class__( # 6 -> Down top right
					cpx + csx, cpy, cpz + csz,
					csx, csy, csz,
					self.__depth + 1),
				self.__class__( # 7 -> Down bottom right
					cpx + csx, cpy, cpz,
					csx, csy, csz,
					self.__depth + 1),
				self.__class__( # 8 -> Down bottom left
					cpx, cpy, cpz,
					csx, csy, csz,
					self.__depth + 1),
				]
			# print(("	" * self.__depth) + str(self))

		##
		## Get self as a string
		##
		def __str__(self):
			return str(self.__x) + " " + str(self.__y) + " " + str(self.__z)

		##
		## Increment one to child counter
		##
		def incrementCounter(self): self.__count += 1

		##
		## Get the child counter
		##
		def getCounter(self): return self.__count

		##
		## Check whether or not a point clips
		## this leaf.
		##
		def clipPoint(self, point):
			return clipPoint(
				(self.__x, self.__y, self.__z),
				(self.__sx, self.__sy, self.__sz),
				point
				)

		##
		## Get a reference to the child leaf nodes
		##
		def getLeafNodes(self): yield self.__leafNodes

		##
		## Get the point of depth of the leaf
		##
		def getDepth(self): return self.__depth

	##
	## Inject a point into the deepest node of an octree
	##
	def injectPoint(self, point):
		__clamp = lambda x, l, h: max(l, min(x, h))
		clampedPoint = (
			__clamp(point[0], -1.0, 1.0),
			__clamp(point[1], -1.0, 1.0),
			__clamp(point[2], -1.0, 1.0)
		)
		node = self.__root # Set node to root
		while node.getDepth() != self.__depth: # Loop until depth is reached
			for leafNodes in node.getLeafNodes(): # For each child node:
				for n in leafNodes: # For each leaf
					## If the point resides in the node,
					## search the children for a deeper clip
					if n.clipPoint(clampedPoint):
						node = n # Set as new node
						break # Exit layer
		## Increment the counter of the deepest octant
		node.incrementCounter()

	##
	## Get the root node of the octree
	##
	def getRoot(self): return self.__root

	##
	## Get the maximum allowed depth of the octree
	##
	def getMaxDepth(self): return self.__depth

	##
	## Recursively add leaf nodes to given node until a depth is reached
	##
	def __populateNode(self, node, maxDepth, depth = 0):
		if depth == maxDepth: return # Exit at depth
		node.populateLeaf()
		## Recursively add nodes
		for nodes in node.getLeafNodes():
			[self.__populateNode(n, maxDepth, depth + 1) for n in nodes]

	##
	## Initialize the octree to a given depth
	##
	def __init__(self, depth):
		self.__root = self.Leaf(
			## Set position to full negative
			px = -1.0,
			py = -1.0,
			pz = -1.0,
			## Set initial size to entire cube
			sx = +1.0,
			sy = +1.0,
			sz = +1.0)
		self.__depth = depth
		self.__populateNode(self.__root, depth)

class MeshOctree(Octree, mesh.Mesh):
	__cube = None
	__voxelPositions = []

	##
	## Recursively add positions based on
	## octree voxel counts.
	##
	def __addPositions(self, x):
		for nodes in x.getLeafNodes():
			for n in nodes:
				if n.getDepth() == Octree.getMaxDepth(self):
					if n.getCounter() >= 1:
						self.__voxelPositions += [n.getOffsetPos()]
				else: self.__addPositions(n)

	##
	## Overridden initializer for the MeshOctree
	##
	def __init__(self, meshFile, depth):
		## Create voxel to instance
		mesh.Mesh.__init__(self, "res/meshes/cube.obj", False)
		mesh.Mesh.getMaterial(self).setSpecFactor(8.0)
		scale = sizeByDepth(depth - 1)
		mesh.Mesh.getTransform(self).scale(scale, scale, scale)
		super(MeshOctree, self).__init__(depth) # Initialize parent class octree
		(verts, _, _,
		_, _, _) = mesh.getOBJData(meshFile) # Load verts
		## Inject verts into octree
		maxVertLen = max([
			math.sqrt(
				abs(v[0] ** 2.0 + v[1] ** 2.0 + v[2] ** 2.0)) for v in verts])
		for v in verts: Octree.injectPoint(
			self,
			(v[0] / maxVertLen, v[1] / maxVertLen, v[2] / maxVertLen))
		self.__addPositions(Octree.getRoot(self))
		mesh.Mesh.setInstanceOffsets(self, self.__voxelPositions)
