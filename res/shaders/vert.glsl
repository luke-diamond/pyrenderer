#version 300 es

precision highp float;

uniform mat4 u_M;
uniform mat4 u_V;
uniform mat4 u_P;

in vec3 in_Pos;
in vec3 in_Norm;
in vec3 in_InstPos;

out vec3 pass_Pos;
out vec3 pass_Norm;

void main() {
	mat4 MVP = u_P * u_V * u_M;
	vec4 pos = MVP * vec4(in_Pos + in_InstPos, 1.0);
	pass_Pos = vec3(u_M * vec4(in_Pos + in_InstPos, 1.0));
	pass_Norm = vec3(u_M * vec4(in_Norm, 0.0));
	gl_Position = pos;
}
