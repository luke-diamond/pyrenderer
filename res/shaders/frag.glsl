#version 300 es

precision highp float;

// Material uniforms
uniform vec3 u_DiffuseColor;
uniform float u_SpecFac;

// Light structure
struct light_Inst {
	vec3 position;
	vec3 color;
	float intensity;
	float attenuation;
};
// Light uniforms
uniform light_Inst u_Lights[16];
uniform int u_LightCount;
uniform float u_LightCountInverse;
uniform vec3 u_AmbientColor;
uniform float u_AmbientIntensity;
uniform vec3 u_Ambient;

uniform float u_Gamma;

// Interpolated attributes
in vec3 pass_Pos;
in vec3 pass_Norm;

out vec4 out_Col;

void main() {
	vec3 norm = normalize(pass_Norm);
	vec3 diffuseColor = vec3(0.0);
	float diffuseFac = 0.0;
	float specularFac = 0.0;
	// Loop through all lights
	for (int i = 0; i < u_LightCount; i++) {
		float dist = distance(u_Lights[i].position, pass_Pos);
		vec3 dir = normalize(u_Lights[i].position - pass_Pos);

		float lightIntensity = max(0.0, dot(norm, dir)) * u_Lights[i].intensity;

		float lightAttenuation = 1.0 / (1.0 + u_Lights[i].attenuation * dist * dist);
		float specularIntensity = pow(max(0.0, dot(dir, normalize(norm))), u_SpecFac);

		diffuseFac += lightIntensity * u_LightCountInverse;
		specularFac += specularIntensity;
		diffuseColor += u_Lights[i].color * vec3(lightIntensity * lightAttenuation);
	}
	vec3 finalCol = diffuseColor * vec3(diffuseFac + specularFac);
	vec3 ambient = u_Ambient;
	out_Col = vec4(pow(finalCol + ambient, vec3(u_Gamma)), 1.0);
}
