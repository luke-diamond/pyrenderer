# Install Instructions:
---
In order to ensure the project runs properly with all required packages, some
pip packages must first be installed.

The packages are:

* PySDL2
	* Manages windows
	* Handles GL context creation
	* Provides input events
	* Enables video
* PyOpenGL
	* For creating modern (OpenGL 3.0 ES+) OpenGL contexts
* Numpy
	* For doing vector/matrix/array math
	* Used for transformations/PyOpenGL data passing

To install these packages on windows, first ensure Python 3 is installed. Then,
run the commands:

	python3 -m pip install pysdl2
	python3 -m pip install pyopengl
	python3 -m pip install numpy

On Linux, ensure that pip3 is installed. On Ubuntu or Debian, this can be done
with the command:

	sudo apt-get install pip3

Once installed, the dependencies can be installed with the command:

	pip3 install pysdl2
	pip3 install pyopengl
	pip3 install numpy

and then to install SDL2:

	sudo apt-get install libsdl2
	sudo apt-get install libsdl2-dev

# Running Instructions
---
**The project requires a minimum of *Python 3.0.0* for the modern features used**

To run the project, use the following commands:
* On Windows, use the command `py main.py` in the project directory.

* On Linux, use the command `python3 main.py` in the project directory.

Alternatively, you can use `./run` or double click on `run.bat` in the project
directory to run the project.
